package Zadanie5;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Set<Integer> numberList = new HashSet<>();
        int[] numbers = new int[]{10,12,10,3,4,12,12,300,12,40,55};

        //a
        for (int i = 0; i <numbers.length ; i++) {
            numberList.add(numbers[i]);
        }

        //b
        System.out.println("Number of elements " + numberList.size());
        //c
        System.out.println("Liczby");
        numberList.stream().forEach(number -> System.out.printf("%d, ",number));
        System.out.println();

        //d
        numberList.remove(10);
        numberList.remove(12);
        System.out.println("Number of elements " + numberList.size());
        System.out.println("Liczby");
        numberList.stream().forEach(number -> System.out.printf("%d, ",number));

        //e
        System.out.println();
        System.out.println(containsDuplicates("Abrakadabra"));

        //f
        Set<ParaLiczb> paraLiczbSet = new HashSet<>();
        paraLiczbSet.add(new ParaLiczb(1,2));
        paraLiczbSet.add(new ParaLiczb(2,1));
        paraLiczbSet.add(new ParaLiczb(1,1));
        paraLiczbSet.add(new ParaLiczb(1,2));

        for (ParaLiczb a: paraLiczbSet) {
            System.out.println(a);
        }



    }

    //h
    public static Set<Double> newSet(double... numbers){

        Set<Double> doubleSet = new HashSet<>();
        for (double e: numbers) {
            doubleSet.add(e);
        }
        return doubleSet;
    }

    private static boolean containsDuplicates(String tekst) {
        System.out.print("Czy zawiera duplikaty: ");
        String[] text = tekst.toLowerCase().split("");
        Set<String> textSet = new HashSet<>();
        textSet.addAll(Arrays.asList(text));

        return text.length != textSet.size();
    }


}
