package Zadanie5;

public class ParaLiczb {

    private int a;
    private int b;

    public ParaLiczb(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParaLiczb paraLiczb = (ParaLiczb) o;

        if (a != paraLiczb.a) return false;
        return b == paraLiczb.b;
    }

    @Override
    public int hashCode() {
        int result = a;
        result = 31 * result + b;
        return result;
    }

    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
