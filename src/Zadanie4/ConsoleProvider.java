package Zadanie4;

import java.util.Scanner;

public class ConsoleProvider implements DataProvider {
    Scanner scanner = new Scanner(System.in);

    @Override
    public int nextInt() {
        System.out.print("Podaj wiek: ");
        int wiek = scanner.nextInt();
        return wiek;
    }

    @Override
    public String nextString() {
        System.out.print("Podaj imie: ");
        String name = scanner.nextLine();
        return name;
    }
}


