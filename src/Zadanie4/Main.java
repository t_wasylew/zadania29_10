package Zadanie4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("What provider to use: ");
        int i = scanner.nextInt();
        DataProvider dataProvider = null;
        if (i == 1) {
            dataProvider = new ConsoleProvider();
        } else if (i == 2) {
            dataProvider = new StaticDataProvider();
        } else if (i == 3) {
            dataProvider = new RandomDataProvider();
        }

        if (dataProvider != null) {
            int wiek = dataProvider.nextInt();
            String name = dataProvider.nextString();
            System.out.println("Wiek " + wiek);
            System.out.println("Imie " + name);
        }
    }
}
