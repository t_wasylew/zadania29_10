package Zadanie2;

import java.util.ArrayList;
import java.util.List;

public class Town {

   private List<Citizen> citizenList = new ArrayList<>();


    public void addCitizen(Citizen citizen) {
        citizenList.add(citizen);
    }

    public int howManyCanVote (){
        int counter = 0;
        for (int i = 0; i < citizenList.size(); i++) {
            if (citizenList.get(i).canVote()) {
                counter++;
            }
        }
        return counter;
    }

}
