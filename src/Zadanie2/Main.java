package Zadanie2;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Citizen citizen1 = new Peasant("Jarek");
        Citizen citizen2 = new Soldier("Tomek");
        Citizen citizen3 = new King("Michal");
        Citizen citizen4 = new Townsman("Olek");

        Town town = new Town();
        town.addCitizen(citizen1);
        town.addCitizen(citizen2);
        town.addCitizen(citizen3);
        town.addCitizen(citizen4);
        System.out.println(town.howManyCanVote());
    }
}
