package Zadanie2;

public class King extends Citizen{
    private String name;

    public King(String name) {
        this.name = name;
    }

    @Override
    public boolean canVote() {
        return false;
    }

}
