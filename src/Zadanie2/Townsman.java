package Zadanie2;

public class Townsman extends Citizen {

    private String name;

    public Townsman(String name) {
        this.name = name;
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
