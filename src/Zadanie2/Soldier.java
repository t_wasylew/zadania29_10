package Zadanie2;

public class Soldier extends Citizen {
    private String name;

    public Soldier(String name) {
        this.name = name;
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
