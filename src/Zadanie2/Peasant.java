package Zadanie2;

public class Peasant extends Citizen {
    private String name;

    public Peasant(String name) {
        this.name = name;
    }

    @Override
    public boolean canVote() {
        return false;
    }

}
