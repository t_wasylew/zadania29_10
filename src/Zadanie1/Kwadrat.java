package Zadanie1;

import Zadanie1.Figure;

public class Kwadrat extends Figure {

    private int a;

    public Kwadrat(int a) {
        this.a = a;
    }

    @Override
    public double obliczPole() {
        return a * a;
    }

    @Override
    public double obliczObwod() {
        return 4*a;
    }
}
