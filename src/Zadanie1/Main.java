package Zadanie1;

import Zadanie1.Kolo;
import Zadanie1.Kwadrat;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Figure kwadrat = new Kwadrat(5);
        Figure prostokat = new Prostokat(6, 5);
        Figure kolo = new Kolo(6);

        System.out.println(kwadrat.obliczPole());
        System.out.println(prostokat.obliczObwod());
        System.out.println(kolo.obliczPole());

        List figure = new ArrayList();
        figure.add(kolo);
        figure.add(kwadrat);
        figure.add(prostokat);

        for (int i = 0; i < figure.size(); i++) {
            if (figure.get(i) instanceof Figure) {
                System.out.println(((Figure) figure.get(i)).obliczObwod());

            }
        }
    }
}
