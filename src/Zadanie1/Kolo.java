package Zadanie1;

import Zadanie1.Figure;

public class Kolo extends Figure {

    private int r;

    public Kolo(int r) {
        this.r = r;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    @Override
    public double obliczPole() {
        return Math.PI * (r * r);
    }

    @Override
    public double obliczObwod() {
        return 2 * Math.PI * r;
    }
}
